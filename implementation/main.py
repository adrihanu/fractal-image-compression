import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from scipy import ndimage
from scipy import optimize
from PIL import Image
import numpy as np
import math
import copy

class Block():
    def __init__(self, img, x=None, y=None, x_max=None, y_max=None):
        self.img = Image.fromarray(img)
        self.x = x
        self.y = y
        self.x_max = x_max
        self.y_max = y_max
        self.transformations = []

    def resize(self, height, width):
        resized_block = copy.copy(self)
        resized_block.img = self.img.resize((height, width))
        return resized_block

    def transform(self, transformation):
        transformed_block = copy.deepcopy(self)
        transformed_block.transformations.append(transformation)
        if transformation is not None:
            transformed_block.img = self.img.transpose(transformation) 
        return transformed_block

    def get_contrast_and_brightness(self, other):
        A = np.array(other.img)
        B = np.array(self.img)
        A = np.concatenate((np.ones((A.size, 1)), np.reshape(A, (A.size, 1))), axis=1)
        B = np.reshape(B, (B.size,))
        brightness, contrast = np.linalg.lstsq(A, B, rcond=None)[0]
        return contrast, brightness

    def get_error(self, other):
        A = np.array(self.img)
        B = np.array(other.img)
        error = ((A - B)**2).mean()
        return error
    
    def __add__(self, other):
        block = copy.deepcopy(self)
        A = np.array(block.img)
        block.img = Image.fromarray(A+other)
        return block
    
    def __rmul__(self, other):
        block = copy.deepcopy(self)
        A = np.array(block.img)
        block.img = Image.fromarray(A*other)
        return block

    def __eq__(self, other):
        return self.img == other.img
    def __ne__(self, other):
        return not self.__eq__(other)
    def __hash__(self):
        return hash(str(np.array(self.img)))




class BlockList(list):
    def __init__(self, *args, **kwargs):
        list.__init__(self, args)
        if 'img' in kwargs:
            step = kwargs['step'] if 'step' in kwargs else None
            self.load_image(kwargs['img'], kwargs['block_size'], step)
        else:
            self.x_max = kwargs['x_max']
            self.y_max = kwargs['y_max']

    def __call__(self, **kwargs):
        self.__dict__.update(kwargs)
        return self

    def get_block(self, x, y):
        for block in self:
            if (block.x, block.y) == (x, y): return block

    def set_block(self, i, new_block):
        self[i].img = new_block.img

    def get_size(self):
        height = width = 0
        for block in self:
            block_height, block_width = block.img.size
            height += block_height
            width += block_width
        return height//self.y_max, width//self.x_max

    def get_image(self):
        img = np.zeros(self.get_size())
        for block in self:
            block_height, block_width = block.img.size
            img[block.y*block_height:(block.y+1)*block_height,
                block.x*block_width:(block.x+1)*block_width ] = block.img
        return img 

    def load_image(self, img, block_size,  step):
        image_height, image_width = img.shape
        if step:
            y_max = (image_height - block_size) // step + 1
            x_max = (image_width - block_size) // step + 1
        else:
            y_max = image_height // block_size
            x_max = image_width // block_size
        self.x_max = x_max
        self.y_max = y_max
        for y in range(y_max):
            for x in range(x_max):
                if step: block_img = img[y*step:y*step+block_size, x*step:x*step+block_size]
                else: block_img = img[y*block_size:(y+1)*block_size, x*block_size:(x+1)*block_size]
                block = Block(block_img, x, y, x_max, y_max)
                self.append(block)

    def plot(self, title="", axis=True):
        plt.figure()
        for i, block in enumerate(self):
            ax = plt.subplot(self.y_max, self.x_max, i+1)
            if not axis: ax.axis("off")
            plt.imshow(block.img)#, cmap='gray', vmin=0, vmax=255, interpolation='none')
        plt.suptitle(title)
        plt.draw()

    def resize(self, height, width):
        resized_blocks = copy.copy(self)
        resized_blocks.clear()
        for block in self:
            resized_blocks.append(block.resize(height, width))
        return resized_blocks

    def transform(self, transformation):
        transformed_blocks = copy.copy(self)
        transformed_blocks.clear()
        for block in self:
            transformed_blocks.append(block.transform(transformation))
        return transformed_blocks
import time
def compress(img="lena.bmp"):

    step = 32
    source_block_size = 32
    destination_block_size = 16

    image_height, image_width = 256, 256

    files_names = ["barbara.bmp", "boats.bmp", "cameraman.bmp", "lighthouse.bmp", "houses.bmp", "trucks.bmp"]
    #files_names = ["cameraman.bmp"]
    #files_names = ["cameraman.bmp"]
    bar = [43.883487898726806,41.04708682567648,40.420866464922625,39.42580869356088,38.73077544812207,37.81387730513277,36.18621754337173,35.409896100118125,34.75816739269742]

    boat= [32.9855113136004,30.72370872543356,29.165943261489996,27.840003566262855,27.18655546105882,27.138294388347262,24.051802537805482,22.692484753699265,22.512254885418002]
    
    camer= [34.20062792310666,32.5851037950226,31.917877473319937,31.47688108096884,31.010154737314224,30.305444734783194,29.25185188179965,28.792983929156595,28.08614981300359]

    ligh = [32.0528664002804,31.603590153300054,30.397448965453293,29.775816176739983,29.58882321217781,29.45478747057259,29.00274392415331,28.454802181770052,28.196317382896513]

    hous = [47.11764383474893,46.978408313647115,46.125322464009784,44.90818636267225,44.594034844485584,43.60895978056154,42.70100317137473,42.01609939934446,41.62151251354742]

    truc = [24.443902508979757,24.303191126066874,23.961188604343217,23.07856975705974,21.87436486607745,20.986202544760907,20.440430673775666,20.005413680449195,19.887971491723896]

    tr =  [2,4,8,16,32,64,128 ,256, 512]
    
    c_tim=[0.59, 0.99,2.28,2.54,4.7,9.4,20.25,40.02, 79.5]

    plt.plot( tr ,bar, '-o', tr, boat, '-o', tr, camer, '-o', tr, ligh, '-o', tr, hous, '-o', tr, truc, '-o')
    plt.legend(['barbara.bmp', 'boat.bmp', 'cameraman.bmp', 'lighthouse.bmp', 'houses.bmp', 'trucks.bmp'])
    plt.xlabel('Liczba transformowanych bloków D')
    plt.ylabel('RMSD')
    plt.show()

    for name in files_names:
        print('\n', name)
        for n_tr in [2,4,8,16,32,64,128 ,256, 512]:
            transformations_1 = [None, Image.FLIP_TOP_BOTTOM, Image.FLIP_LEFT_RIGHT, Image.TRANSPOSE]
            transformations_2 = [None, Image.ROTATE_90, Image.ROTATE_180, Image.ROTATE_270]
            transformations = [(t1, t2) for t1 in transformations_1 for t2 in transformations_2]
            #print('\ntransformed blocks', n_tr)

            #ax = plt.subplot(2, 3, i+1)
            #ax.axis("off")
            co_time = time.time()
            img = Image.open('../thesis/images/'+name)
            img = img.resize((image_width, image_height))

            #plt.imshow(img)#, cmap='gray', vmin=0, vmax=255, interpolation='none')
            #plt.title(name)# RMSD: {:.2f}".format(i+1, error))

            #plt.suptitle("Obrazy testowe")# RMSD: {:.2f}".format(i+1, error))
        
            img = np.array(img, dtype=np.int32)
            #plt.imshow(img)



            source_blocks = BlockList(img=img, block_size=source_block_size, step=step)
            destination_blocks = BlockList(img=img, block_size=destination_block_size)
            resized_source_blocks = source_blocks.resize(destination_block_size, destination_block_size)


            transformed_blocks = BlockList(x_max=source_blocks.x_max*source_blocks.y_max, 
                                           y_max=len(transformations))
            for transformation in transformations:
                transformed_blocks += resized_source_blocks.transform(transformation[0]).transform(transformation[1])
                if len(transformed_blocks)>n_tr: break
            transformed_blocks=transformed_blocks[:n_tr]
            compressed = []
            for destination_block in destination_blocks:
                #print("Destination block(x={}, y={})".format(destination_block.x, destination_block.y))
                min_error = float('inf')
                for transformed_block in transformed_blocks:
                    contrast, brightness = destination_block.get_contrast_and_brightness(transformed_block)
                    transformed_block = contrast*transformed_block + brightness
                    error = destination_block.get_error(transformed_block)
                    if error < min_error:
                        min_error = error
                        best_transformed_block = transformed_block
                        best_transformed_block.contrast = contrast
                        best_transformed_block.brightness = brightness
                compressed.append((best_transformed_block.x, best_transformed_block.y, 
                                   best_transformed_block.contrast, best_transformed_block.brightness,
                                    best_transformed_block.transformations))

            #print("com time: {}s".format(time.time()-co_time))
            ################################################################################################
            ################################################################################################
            ################################################################################################
            init_img = np.random.randint(0, 256, (image_height, image_width), np.int32)

            init_iteration_block_list = BlockList(img=init_img, block_size=destination_block_size)
            iterations = [init_iteration_block_list]
            
            empty_block_list = copy.deepcopy(init_iteration_block_list)
            #empty_block_list.clear()
            de_time=time.time()
            iterations_number = 10 
            for i in range(iterations_number-1):
                #print('Iteration', i)
                last_iteration_img = iterations[-1].get_image()
                last_iteration_source_block_list = BlockList(img=last_iteration_img, block_size=source_block_size)
                new_iteration_destination_block_list = copy.copy(empty_block_list)
                indx=0
                for x, y, contrast, brightness, transformations in compressed:
                    #print("\tx,y:", x, y, "Transformations:",transformations, brightness, contrast)
                    source_block = last_iteration_source_block_list.get_block(x,y)
                    resized_block = source_block.resize(destination_block_size, destination_block_size)
                    transformed_block = resized_block.transform(transformations[0])
                    transformed_block = transformed_block.transform(transformations[1])
                    transformed_block = contrast*transformed_block + brightness

                    new_iteration_destination_block_list.set_block(indx, transformed_block)
                    indx+=1
                iterations.append(copy.deepcopy(new_iteration_destination_block_list))

            plt.figure()
            for i, iteration in enumerate(iterations):
                ax = plt.subplot(4, 4, i+1)
                ax.axis("off")
                plt.imshow(iteration.get_image(), cmap='gray', vmin=0, vmax=255, interpolation='none')
                
                error = np.sqrt(np.mean(np.square(img - iteration.get_image())))
                if i==iterations_number-1: print(error, end=',')
                plt.title("Iteracja {} RMSD: {:.2f}".format(i+1, error))
            plt.suptitle('Iteracje')
            #print("Decompression time:", time.time()-de_time )
            #source_blocks.plot(title="Source blocks")
            #resized_source_blocks.plot(title="Resized blocks")
            #destination_blocks.plot(title="Destination blocks")
            #transformed_blocks.plot(title="Transformed blocks")

    #plt.show()
if __name__ == '__main__':
    compress()
