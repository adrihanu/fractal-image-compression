# Fractal compression of natural images

The paper contains a theoretical description of selected fractal compression algorithms.

The `implementation` directory contains an implemented fractal compression algorithm.

The algorithm was tested using a set of natural images, and the obtained results were compared to the JPEG standard.

Tags: `Python`, `Fractal compression`, `SciPy`, `NumPy`, `R Markdown`